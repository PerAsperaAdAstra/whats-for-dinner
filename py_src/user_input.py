#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Below, put your own OpenWeather API Key inside the quotes.
You can get one for free here: https://openweathermap.org/
'''
open_weather_api = "YOUR_API_KEY"

'''
Below, put your City and Country Abbreviation inside the quotes.
Here's the example for New York:
weather_city = "New York"
weather_country = "USA"
weather_latitude = "40.748"
weather_longitude = "-73.985"

Not sure about your lat/long?  Head over to Google maps and 
touch a place on the ground.  It should give you lat/long!
'''
weather_city = "YOUR_CITY"
weather_country = "YOUR_COUNTRY"
weather_latitude = "##.###"
weather_longitude = "##.###"