#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
recipe_unit_test.py

This program is intended to compare all combinations
of recipes to identify and locate errors

Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

###########################
#IMPORT MODULES
###########################
import os
import json




###########################
#VARIABLES
###########################
background_color = 'black'


#load recipes
recipe_data_list = []
recipe_name_list = []
recipe_list = [x  for x in os.listdir('../recipes') if x.endswith(('.json'))]
incompatible_units = []
#print(recipe_list)


for filename in recipe_list:
    #print(filename)
    with open(os.path.join('../recipes',filename)) as f:
        #print(f)
        recipe_data_list += [json.load(f)]
        recipe_name_list += [recipe_data_list[-1]['name']]


for n in range(0,len(recipe_data_list)):
    print(recipe_data_list[n]['name'])
    recipe1 = recipe_data_list[n]
    for ii in range(0,len(recipe_data_list)):
        recipe2 = recipe_data_list[ii]
        print(recipe_data_list[ii]['name'])


#create list
        grocery_list = {}
        keys = list(recipe1['ingredients'].keys())
        keys+= list(recipe2['ingredients'].keys())
        keys = list(set(keys))
        for key in keys:
            #print(key)
            if key in recipe1['ingredients'] and key in recipe2['ingredients']:
                #print(key)
                ingr1 = recipe1['ingredients'][key].split(' ')
                ingr2 = recipe2['ingredients'][key].split(' ')

                if len(ingr1) < 2: units1 = None
                else: units1 = recipe1['ingredients'][key].split(' ')[1]

                if len(ingr2) < 2: units2 = None
                else: units2 = recipe2['ingredients'][key].split(' ')[1]

                amount1 = float(recipe1['ingredients'][key].split(' ')[0])
                amount2 = float(recipe2['ingredients'][key].split(' ')[0])

                if units1 == 'cup':
                    units1 = 'cups'
                if units2 == 'cup':
                    units2 = 'cups'

                if units1 == 'tsp':
                    units1 = 'cups'
                    amount1=amount1*0.0208333
                if units2 == 'tsp':
                    units2 = 'cups'
                    amount2=amount2*0.0208333

                if units1 == 'tbs':
                    units1 = 'cups'
                    amount1=amount1*0.0625

                if units2 == 'tbs':
                    units2 = 'cups'
                    amount2=amount2*0.0625

                if units1 == 'cups':
                    units1 = 'oz'
                    amount1=amount1*8

                if units2 == 'cups':
                    units2 = 'oz'
                    amount2=amount2*8

                if (units1 == units2) or (units1 in units2 or units2 in units1):
                    if units1 == None: units1 = ''
                    else: units1 = ' ' +units1
                    grocery_list[key] = str(round(amount1+amount2,2))+units1
                else:
                    grocery_list[key] = [recipe1['ingredients'][key],recipe2['ingredients'][key]]
                    print('\n\n FOUND INCOMPATIBLE UNITS  \n\n')
                    incompatible_units += [recipe1,recipe2]

            elif key in recipe1['ingredients']:
                grocery_list[key] = recipe1['ingredients'][key]
            elif key in recipe2['ingredients']:
                grocery_list[key] = recipe2['ingredients'][key]


        #print(recipe1['name'])
        #print(recipe1['ingredients'])
        #print(' ')
        #print(recipe2['name'])
        #print(recipe2['ingredients'])

        print('\nCombined List: ')
        print(grocery_list)

if len(incompatible_units) < 1:
    print('\n\n All Recipes Pass!\n\n')
else:
    print('\n\n Incompatible units found!  Fix them before proceeding.')
    print(incompatible_units)


