#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
draw_weather_screen.py

icons from: https://icons8.com/icon/set/weather/ios

This program is intended to draw a screen showing weather information

Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

###########################
#IMPORT MODULES
###########################
import time
import json
import numpy as np
from open_weather import update_weather
from PIL import Image, ImageDraw, ImageFont


###########################
#VARIABLES
###########################
background_color = (24,116,205)
text_color = (255,255,255)
icon_color = [255,255,255]
W, H = (1920,1080)
project_font = "fonts/LiberationSans-Regular.ttf"


font36 = ImageFont.truetype(project_font, 36)
font48 = ImageFont.truetype(project_font, 48)
font96 = ImageFont.truetype(project_font, 96)
font100 = ImageFont.truetype(project_font, 100)
font125 = ImageFont.truetype(project_font, 125)
font150 = ImageFont.truetype(project_font, 150)
font300 = ImageFont.truetype(project_font, 300)



###########################
# SUPPORT FUNCTIONS
###########################
def draw_icon(the_im, x, y, l, h, icon):

    im_icon = Image.open("icons/" + icon + ".png")
    im_icon = im_icon.resize((l, h))

    data = np.array(im_icon)
    r1, g1, b1 = 0, 0, 0 # Original value
    r2, g2, b2 = icon_color[0], icon_color[1], icon_color[2] # Value that we want to replace it with

    red, green, blue = data[:,:,0], data[:,:,1], data[:,:,2]
    mask = (red == r1) & (green == g1) & (blue == b1)
    data[:,:,:3][mask] = [r2, g2, b2]

    im_icon = Image.fromarray(data)

    #im_icon.show()

    the_im.paste(im_icon, (x, y), im_icon)


def draw_real_weather_screen(weather_report, im1):

    #im1 = Image.new('RGB', (W, H), background_color)
    draw_im1 = ImageDraw.Draw(im1)


    #last update
    update_str = "Last updated " + weather_report['last_update_time']
    w, h = draw_im1.textsize(update_str, font=font36)
    draw_im1.text(((W-w)/2-30,(H-h)/2+450), update_str, fill=text_color, font=font36)

    #draw current conditions
    draw_icon(im1, 960-230, 400-130, 400, 400, weather_report['current_icon'])
    w, h = draw_im1.textsize(str(weather_report['current_temp'])+"F", font=font150)
    draw_im1.text(((W-w)/2-30,(H-h)/2+190), str(weather_report['current_temp'])+"F", fill=text_color, font=font150)
    w, h = draw_im1.textsize(str(weather_report['current_status']), font=font125)
    draw_im1.text(((W-w)/2-30,(H-h)/2+340), str(weather_report['current_status']), fill=text_color, font=font125)

    #draw sunrise and sunset
    draw_icon(im1, 525, 115+330, 125, 125, 'sunrise')
    sunrise_time = weather_report['sunrise']
    if sunrise_time[0] == "0": sunrise_time = sunrise_time[1:]
    draw_im1.text((525+15, 115+330+140), sunrise_time, fill=text_color, font=font48)

    draw_icon(im1, 525+700, 115+330, 125, 125, 'sunset')
    sunset_time = weather_report['sunset']
    if sunset_time[0] == "0": sunset_time = sunset_time[1:]
    draw_im1.text((525+15+700, 115+330+140), sunset_time, fill=text_color, font=font48)


    #draw next few hours
    draw_im1.text((1450-1350, 30), weather_report['h+2_name'], fill=text_color, font=font96)
    draw_icon(im1, 1440-1350, 115, 150, 150, weather_report['h+2_icon'])
    draw_im1.text((1600-1350, 140), str(weather_report['h+2_temp']), fill=text_color, font=font48)
    draw_im1.text((1650-1350, 140), "F", fill=text_color, font=font48)
    draw_im1.text((1450-1350, 250), weather_report['h+2_status'], fill=text_color, font=font48)

    draw_im1.text((1450-1350, 30+330), weather_report['h+4_name'], fill=text_color, font=font96)
    draw_icon(im1, 1440-1350, 115+330, 150, 150, weather_report['h+4_icon'])
    draw_im1.text((1600-1350, 140+330), str(weather_report['h+4_temp']), fill=text_color, font=font48)
    draw_im1.text((1650-1350, 140+330), "F", fill=text_color, font=font48)
    draw_im1.text((1450-1350, 250+330), weather_report['h+4_status'], fill=text_color, font=font48)

    draw_im1.text((1450-1350, 30+680), weather_report['h+6_name'], fill=text_color, font=font96)
    draw_icon(im1, 1440-1350, 115+680, 150, 150, weather_report['h+6_icon'])
    draw_im1.text((1600-1350, 140+680), str(weather_report['h+6_temp']), fill=text_color, font=font48)
    draw_im1.text((1650-1350, 140+680), "F", fill=text_color, font=font48)
    draw_im1.text((1450-1350, 250+680), weather_report['h+6_status'], fill=text_color, font=font48)


    #draw next few days
    draw_im1.text((1450+20, 30), weather_report['d+1_name'], fill=text_color, font=font96)
    draw_icon(im1, 1440+20, 115, 150, 150, weather_report['d+1_icon'])
    draw_im1.text((1600+20, 140), str(weather_report['d+1_temp_min']), fill=text_color, font=font48)
    draw_im1.text((1650+20, 140), "F min", fill=text_color, font=font48)
    draw_im1.text((1600+20, 190), str(weather_report['d+1_temp_max']), fill=text_color, font=font48)
    draw_im1.text((1650+20, 190), "F max", fill=text_color, font=font48)
    draw_im1.text((1450+20, 250), weather_report['d+1_status'], fill=text_color, font=font48)

    draw_im1.text((1450+20, 30+330), weather_report['d+2_name'], fill=text_color, font=font96)
    draw_icon(im1, 1440+20, 115+330, 150, 150, weather_report['d+2_icon'])
    draw_im1.text((1600+20, 140+330), str(weather_report['d+2_temp_min']), fill=text_color, font=font48)
    draw_im1.text((1650+20, 140+330), "F min", fill=text_color, font=font48)
    draw_im1.text((1600+20, 190+330), str(weather_report['d+2_temp_max']), fill=text_color, font=font48)
    draw_im1.text((1650+20, 190+330), "F max", fill=text_color, font=font48)
    draw_im1.text((1450+20, 250+330), weather_report['d+2_status'], fill=text_color, font=font48)

    draw_im1.text((1450+20, 30+680), weather_report['d+3_name'], fill=text_color, font=font96)
    draw_icon(im1, 1440+20, 115+680, 150, 150, weather_report['d+3_icon'])
    draw_im1.text((1600+20, 140+680), str(weather_report['d+3_temp_min']), fill=text_color, font=font48)
    draw_im1.text((1650+20, 140+680), "F min", fill=text_color, font=font48)
    draw_im1.text((1600+20, 190+680), str(weather_report['d+3_temp_max']), fill=text_color, font=font48)
    draw_im1.text((1650+20, 190+680), "F max", fill=text_color, font=font48)
    draw_im1.text((1450+20, 250+680), weather_report['d+3_status'], fill=text_color, font=font48)

    im1.save("weather.png")


def draw_fail_screen(im1):

    #im1 = Image.new('RGB', (W, H), background_color)
    draw_im1 = ImageDraw.Draw(im1)

    update_str = ':('
    w, h = draw_im1.textsize(update_str, font=font300)
    draw_im1.text(((W-w)/2,(H-h)/2-90), update_str, fill=text_color, font=font300)

    update_str = 'Unable to'
    w, h = draw_im1.textsize(update_str, font=font150)
    draw_im1.text(((W-w)/2,(H-h)/2+160), update_str, fill=text_color, font=font150)

    update_str = 'access weather'
    w, h = draw_im1.textsize(update_str, font=font150)
    draw_im1.text(((W-w)/2,(H-h)/2+300), update_str, fill=text_color, font=font150)

    im1.save("weather.png")


def draw_weather_screen():

    time_for_an_update = True

    # load last data set
    try:
        with open('weather_report.json') as json_file:
            weather_report = json.load(json_file)

        #if weather has been updated in last 10 minutes, don't do it again
        last_update_time = weather_report['last_update_sys_time']
        if (time.time() - last_update_time) < (10*60): time_for_an_update = False
    except: pass

    weather_good = True
    if time_for_an_update:
        try: weather_report = update_weather()
        except: weather_good = False

    #draw current time
    im1 = Image.new('RGB', (W, H), background_color)
    hour_min = time.strftime('%I:%M %p', time.localtime(time.time()))
    if hour_min[0] == "0": hour_min = hour_min[1:]
    month_day = time.strftime('%a, %b %d', time.localtime(time.time()))
    w, h = ImageDraw.Draw(im1).textsize(month_day, font=font100)
    ImageDraw.Draw(im1).text(((W-w)/2-30,(H-h)/2-460), month_day, fill=text_color, font=font100)
    w, h = ImageDraw.Draw(im1).textsize(hour_min, font=font150)
    ImageDraw.Draw(im1).text(((W-w)/2-30,(H-h)/2-340), hour_min, fill=text_color, font=font150)


    if weather_good == True: draw_real_weather_screen(weather_report,im1)
    else: draw_fail_screen(im1)


###########################
#MAIN CODE
###########################

if __name__ == '__main__':

    draw_weather_screen()

    im1 = Image.open(r"weather.png")  
    im1.show()


