#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
open_weather.py

This program is intended to call, parse, and return various
weather parameters gathered from the Open Weather API

Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

###########################
#IMPORT MODULES
###########################
import time
import json
import requests
from user_input import open_weather_api, weather_latitude, weather_longitude, weather_city, weather_country


###########################
# SUPPORT FUNCTIONS
###########################
def get_icon(id):

    icon = 'unknown'

    if id == 800 or id == 801 or id == 802:
        icon = 'clear_day'
    if id == 802 or id == 803:
        icon = 'partly_cloudy'
    if id == 804:
        icon = 'cloudy'
    if id > 700 and id < 800:
        icon = 'fog'
    if id > 599 and id < 700:
        icon = 'snow'
    if id > 499 and id < 600:
        icon = 'rain'
    if id > 299 and id < 400:
        icon = 'rain'
    if id > 199 and id < 300:
        icon = 'thunderstorm'

    return icon



def update_weather():

    data = requests.get(f"https://api.openweathermap.org/data/2.5/onecall?lat={weather_latitude}&lon={weather_longitude}&appid={open_weather_api}&units=imperial").json()

    if False:
        for key in data.keys():
            print(key)
            print(data[key])
            print('-------------------------------------------------')


    weather_report = {}

    #grab current conditions
    weather_report['last_update_time'] = time.strftime("%m/%d/%Y %H:%M", time.localtime(data["current"]["dt"]))
    weather_report['last_update_raw_time'] = data["current"]["dt"]
    weather_report['last_update_sys_time'] = time.time()
    weather_report['current_temp'] = round(data["current"]['temp'])
    weather_report['current_status'] = data["current"]['weather'][0]['description'].title()
    if weather_report['current_status'] == 'Overcast Clouds' or weather_report['current_status'] == 'Scattered Clouds': 
        weather_report['current_status'] = 'Clouds'
    weather_report['current_icon'] = get_icon(data["current"]['weather'][0]['id'])
    weather_report['sunrise'] = time.strftime("%I:%M", time.localtime(data["current"]["sunrise"]))
    weather_report['sunset'] = time.strftime("%I:%M", time.localtime(data["current"]["sunset"]))
    if data["current"]["sunset"] < data["current"]["dt"] and weather_report['current_icon'] == 'clear_day': weather_report['current_icon'] = 'clear_night'


    #grab +2 hour conditions
    weather_report['h+2_temp'] = round(data["hourly"][2]['temp'])
    weather_report['h+2_status'] = data["hourly"][2]['weather'][0]['description']
    weather_report['h+2_icon'] = get_icon(data["hourly"][2]['weather'][0]['id'])
    if data["current"]["sunset"] < data["hourly"][2]["dt"] and weather_report['h+2_icon'] == 'clear_day': weather_report['h+2_icon'] = 'clear_night'
    weather_report['h+2_name'] = time.strftime('%I %p', time.localtime(data["hourly"][2]["dt"]))

    #grab +4 hour conditions
    weather_report['h+4_temp'] = round(data["hourly"][4]['temp'])
    weather_report['h+4_status'] = data["hourly"][4]['weather'][0]['description']
    weather_report['h+4_icon'] = get_icon(data["hourly"][4]['weather'][0]['id'])
    if data["current"]["sunset"] < data["hourly"][4]["dt"] and weather_report['h+4_icon'] == 'clear_day': weather_report['h+4_icon'] = 'clear_night'
    weather_report['h+4_name'] = time.strftime('%I %p', time.localtime(data["hourly"][4]["dt"]))

    #grab +6 hour conditions
    weather_report['h+6_temp'] = round(data["hourly"][6]['temp'])
    weather_report['h+6_status'] = data["hourly"][6]['weather'][0]['description']
    weather_report['h+6_icon'] = get_icon(data["hourly"][6]['weather'][0]['id'])
    if data["current"]["sunset"] < data["hourly"][6]["dt"] and weather_report['h+6_icon'] == 'clear_day': weather_report['h+6_icon'] = 'clear_night'
    weather_report['h+6_name'] = time.strftime('%I %p', time.localtime(data["hourly"][6]["dt"]))



    #grab +1 day conditions
    weather_report['d+1_temp_min'] = round(data["daily"][1]['temp']['min'])
    weather_report['d+1_temp_max'] = round(data["daily"][1]['temp']['max'])
    weather_report['d+1_status'] = data["daily"][1]['weather'][0]['description']
    weather_report['d+1_icon'] = get_icon(data["daily"][1]['weather'][0]['id'])
    weather_report['d+1_name'] = time.strftime('%a', time.localtime(data["daily"][1]["dt"]))

    #grab +2 day conditions
    weather_report['d+2_temp_min'] = round(data["daily"][2]['temp']['min'])
    weather_report['d+2_temp_max'] = round(data["daily"][2]['temp']['max'])
    weather_report['d+2_status'] = data["daily"][2]['weather'][0]['description']
    weather_report['d+2_icon'] = get_icon(data["daily"][2]['weather'][0]['id'])
    weather_report['d+2_name'] = time.strftime('%a', time.localtime(data["daily"][2]["dt"]))

    #grab +3 day conditions
    weather_report['d+3_temp_min'] = round(data["daily"][3]['temp']['min'])
    weather_report['d+3_temp_max'] = round(data["daily"][3]['temp']['max'])
    weather_report['d+3_status'] = data["daily"][3]['weather'][0]['description']
    weather_report['d+3_icon'] = get_icon(data["daily"][3]['weather'][0]['id'])
    weather_report['d+3_name'] = time.strftime('%a', time.localtime(data["daily"][3]["dt"]))

    with open('weather_report.json', 'w') as f:
        json.dump(weather_report, f, indent=4)

    return weather_report


###########################
#MAIN CODE
###########################

if __name__ == "__main__":
    weather_report = update_weather()

    print(weather_report)

