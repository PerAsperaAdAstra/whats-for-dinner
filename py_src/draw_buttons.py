#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
draw_buttons.py

icons from: https://icons8.com

This program is intended to draw icons for home screen buttons

Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

###########################
#IMPORT MODULES
###########################
import os
import numpy as np
from PIL import Image, ImageDraw, ImageFont


###########################
#VARIABLES
###########################
background_color = (100,100,100)
background_color_blue = (24,116,205)
text_color = (255,255,255)
icon_color = [255,255,255]
W, H = (640,330)
project_font = "fonts/LiberationSans-Regular.ttf"


font36 = ImageFont.truetype(project_font, 36)
font48 = ImageFont.truetype(project_font, 48)
font80 = ImageFont.truetype(project_font, 80)
font96 = ImageFont.truetype(project_font, 96)
font100 = ImageFont.truetype(project_font, 100)
font125 = ImageFont.truetype(project_font, 125)


###########################
# SUPPORT FUNCTIONS
###########################
def draw_icon(the_im, x, y, l, h, icon):

    im_icon = Image.open("icons/" + icon + ".png")
    im_icon = im_icon.resize((l, h))

    data = np.array(im_icon)
    r1, g1, b1 = 0, 0, 0 # Original value
    r2, g2, b2 = icon_color[0], icon_color[1], icon_color[2] # Value that we want to replace it with

    red, green, blue = data[:,:,0], data[:,:,1], data[:,:,2]
    mask = (red == r1) & (green == g1) & (blue == b1)
    data[:,:,:3][mask] = [r2, g2, b2]

    im_icon = Image.fromarray(data)

    #im_icon.show()

    the_im.paste(im_icon, (x, y), im_icon)


def draw_button_icons():

    #draw start cookin button
    if os.path.exists("start_cookin_button.png"): os.remove("start_cookin_button.png")

    start_cookin_button = Image.new('RGB', (W, H), background_color)
    draw_start_cookin_button = ImageDraw.Draw(start_cookin_button)

    draw_icon(start_cookin_button, 220, 30, 175, 175, 'pot')
    w, h = draw_start_cookin_button.textsize("Start Cookin'", font=font100)
    draw_start_cookin_button.text(((W-w)/2,(H-h)/2+100), "Start Cookin'", fill=text_color, font=font100)

    start_cookin_button.save("start_cookin_button.png")

    #draw timer button
    if os.path.exists("timer_button.png"): os.remove("timer_button.png")

    timer_button = Image.new('RGB', (W, H), background_color)
    draw_timer_button = ImageDraw.Draw(timer_button)

    draw_icon(timer_button, 220, 30, 175, 175, 'timer')
    w, h = draw_timer_button.textsize("Timer", font=font100)
    draw_timer_button.text(((W-w)/2,(H-h)/2+100), "Timer", fill=text_color, font=font100)

    timer_button.save("timer_button.png")

    #draw cast button
    if os.path.exists("cast_button.png"): os.remove("cast_button.png")

    cast_button = Image.new('RGB', (W, H), background_color)
    draw_timer_button = ImageDraw.Draw(cast_button)

    draw_icon(cast_button, 220, 30, 175, 175, 'cast')
    w, h = draw_timer_button.textsize("RpiPlay", font=font100)
    draw_timer_button.text(((W-w)/2,(H-h)/2+100), "RpiPlay", fill=text_color, font=font100)

    cast_button.save("cast_button.png")


    #draw create recipe button
    if os.path.exists("create_recipe_button.png"): os.remove("create_recipe_button.png")

    create_recipe_button = Image.new('RGB', (W, H), background_color)
    draw_create_recipe_button = ImageDraw.Draw(create_recipe_button)

    draw_icon(create_recipe_button, 220, 30, 175, 175, 'new_doc')
    w, h = draw_create_recipe_button.textsize("Create Recipe", font=font80)
    draw_create_recipe_button.text(((W-w)/2,(H-h)/2+100), "Create Recipe", fill=text_color, font=font80)

    create_recipe_button.save("create_recipe_button.png")

    #draw weather button
    if os.path.exists("weather_button.png"): os.remove("weather_button.png")

    weather_button = Image.new('RGB', (W, H), background_color_blue)
    draw_weather_button = ImageDraw.Draw(weather_button)

    draw_icon(weather_button, 160, 0, 250, 250, 'partly_cloudy')
    w, h = draw_weather_button.textsize("Weather", font=font100)
    draw_weather_button.text(((W-w)/2,(H-h)/2+100), "Weather", fill=text_color, font=font100)

    weather_button.save("weather_button.png")

    #draw shopping list button
    if os.path.exists("shopping_list_button.png"): os.remove("shopping_list_button.png")

    shopping_list_button = Image.new('RGB', (W, H), background_color)
    draw_shopping_list_button = ImageDraw.Draw(shopping_list_button)

    draw_icon(shopping_list_button, 220, 30, 175, 175, 'cart')
    w, h = draw_shopping_list_button.textsize("Shopping List", font=font80)
    draw_shopping_list_button.text(((W-w)/2,(H-h)/2+100), "Shopping List", fill=text_color, font=font80)

    shopping_list_button.save("shopping_list_button.png")


    #draw update button
    if os.path.exists("update_button.png"): os.remove("update_button.png")

    update_button = Image.new('RGB', (W, H), background_color)
    draw_update_button = ImageDraw.Draw(update_button)

    draw_icon(update_button, 220, 10, 175, 175, 'update')
    w, h = draw_update_button.textsize("Update", font=font100)
    draw_update_button.text(((W-w)/2,(H-h)/2+100), "Update", fill=text_color, font=font100)

    update_button.save("update_button.png")

    #draw restart button
    if os.path.exists("restart_button.png"): os.remove("restart_button.png")

    restart_button = Image.new('RGB', (W, H), background_color)
    draw_restart_button = ImageDraw.Draw(restart_button)

    draw_icon(restart_button, 220, 30, 175, 175, 'restart')
    w, h = draw_restart_button.textsize("Restart", font=font100)
    draw_restart_button.text(((W-w)/2,(H-h)/2+100), "Restart", fill=text_color, font=font100)

    restart_button.save("restart_button.png")

    #draw up button
    if os.path.exists("up_button.png"): os.remove("up_button.png")

    up_button = Image.new('RGB', (W-300, H-100), background_color)
    draw_up_button = ImageDraw.Draw(up_button)
    draw_icon(up_button, 90, 30, 175, 175, 'up')
    #w, h = draw_up_button.textsize("Restart", font=font100)
    #draw_up_button.text(((W-w)/2,(H-h)/2+100), "Restart", fill=text_color, font=font100)
    up_button.save("up_button.png")

    #draw down button
    if os.path.exists("down_button.png"): os.remove("down_button.png")

    down_button = Image.new('RGB', (W-300, H-100), background_color)
    draw_down_button = ImageDraw.Draw(down_button)
    draw_icon(down_button, 90, 30, 175, 175, 'down')
    #w, h = draw_down_button.textsize("Restart", font=font100)
    #draw_down_button.text(((W-w)/2,(H-h)/2+100), "Restart", fill=text_color, font=font100)
    down_button.save("down_button.png")


    #draw pick for me button
    if os.path.exists("pick_for_me_button.png"): os.remove("pick_for_me_button.png")

    pick_for_me_button = Image.new('RGB', (W, H+300), background_color)
    draw_pick_for_me_button = ImageDraw.Draw(pick_for_me_button)

    draw_icon(pick_for_me_button, 220, 30+100, 175, 175, 'ambivalent')
    w, h = draw_pick_for_me_button.textsize("Pick For Me", font=font100)
    draw_pick_for_me_button.text(((W-w)/2,(H-h)/2+300), "Pick For Me", fill=text_color, font=font100)

    pick_for_me_button.save("pick_for_me_button.png")


    #draw select recipe button
    if os.path.exists("select_recipe_button.png"): os.remove("select_recipe_button.png")

    select_recipe_button = Image.new('RGB', (W, H+300), background_color)
    draw_select_recipe_button = ImageDraw.Draw(select_recipe_button)

    draw_icon(select_recipe_button, 220, 30+100, 175, 175, 'select')
    w, h = draw_select_recipe_button.textsize("Select Recipe", font=font100)
    draw_select_recipe_button.text(((W-w)/2,(H-h)/2+300), "Select Recipe", fill=text_color, font=font100)

    select_recipe_button.save("select_recipe_button.png")


###########################
#MAIN CODE
###########################

if __name__ == '__main__':

    draw_button_icons()



