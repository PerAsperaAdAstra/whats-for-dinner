#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
whats_for_dinner.py

This program is intended to capture recipes, provide options for selecting
them, and then assemble grocery lists

Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

###########################
#IMPORT MODULES
###########################
import sys
import os
import time
import json
import random
import platform
import tkinter as tk #python 3
from draw_buttons import draw_button_icons
from tkinter.scrolledtext import ScrolledText
from user_input import open_weather_api, weather_city, weather_country
from draw_weather_screen import draw_weather_screen



###########################
#VARIABLES
###########################
background_color = 'black'


###########################
#CLASSES
###########################

#GUI class
class theGUI:

    def __init__(self, master):

        # set vars
        self.master = master
        master.title("What's For Dinner?!")

        if platform.system() == 'Windows':
            #set font
            self.spacer_font = 'Helvetica 1 bold'
            self.standard_font = 'Helvetica 60 bold'
            self.small_font = 'Helvetica 30 bold'
            self.big_font = 'Helvetica 100 bold'
            self.huge_font = 'Helvetica 150 bold'
        else:
            #set font
            self.spacer_font = 'Helvetica 1 bold'
            self.standard_font = 'Helvetica 60 bold'
            self.small_font = 'Helvetica 30 bold'
            self.big_font = 'Helvetica 100 bold'
            self.huge_font = 'Helvetica 150 bold'
            #start RPiPlay
            py_src = os.path.dirname(os.path.realpath(__file__))
            repo_dir = os.path.dirname(py_src)
            home_dir = os.path.dirname(repo_dir)
            os.chdir(home_dir)
            os.chdir('RPiPlay/build')
            os.system('./rpiplay -b "off" &')
            os.chdir(py_src)


        #full file dir
        self.full_file_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
        os.chdir(self.full_file_dir)

        #draw button icons
        draw_button_icons()

        #set vars
        self.show_weather_flag = True
        self.continue_timer_flag = True
        self.hr_display_text = tk.StringVar()
        self.hr_display_text.set("00")
        self.min_display_text = tk.StringVar()
        self.min_display_text.set("00")
        self.sec_display_text = tk.StringVar()
        self.sec_display_text.set("00")

        #load recipes
        self.recipe_list = [x  for x in os.listdir('../recipes') if x.endswith(('.json'))]
        #print(self.recipe_list)

        self.recipe_data_list = []
        self.recipe_name_list = []
        self.button_text = tk.StringVar()
        self.photo=tk.PhotoImage()

        for filename in self.recipe_list:
                #print(filename)
                with open(os.path.join('../recipes',filename)) as f:
                    #print(f)
                    self.recipe_data_list += [json.load(f)]
                    self.recipe_name_list += [self.recipe_data_list[-1]['name']]

        self.ri = 0

        #print(recipe_data_list)

        # draw start page
        self.draw_start_screen()


    # weather screen
    def weather_window(self):

        if self.show_weather_flag:
            self.show_weather_flag = False

            # window stuff
            self.newWindow = tk.Toplevel(self.master)
            self.newWindow.title("Weather") 

            # sets the geometry of toplevel 
            self.newWindow.geometry("1920x1080")

            try:  os.remove("weather.png")
            except: pass
            draw_weather_screen()
            self.photo=tk.PhotoImage(file="weather.png")

            self.the_button = tk.Button(self.newWindow, text=(), image=self.photo, font=self.huge_font, command=self.close_weather_window )

            self.the_button.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
            self.the_button.config(bg='DodgerBlue3',fg='sky blue',activebackground = "DodgerBlue3",activeforeground = "sky blue")

            self.update_weather_button()

        else: pass


    def update_weather_button(self):

        try:  os.remove("weather.png")
        except: pass
        draw_weather_screen()
        self.photo=tk.PhotoImage(file="weather.png")
        self.the_button.config(image=self.photo)

        self.master.after(1*60000, self.update_weather_button) #update every 1 min, 1*60000ms


    def close_weather_window(self):

        self.show_weather_flag = True
        self.newWindow.destroy()


    # step 1, pick stuff
    def draw_start_screen(self):

        try:
            self.back_to_start_button.grid_forget()
            self.spacer1.grid_forget()
            self.spacer2.grid_forget()
            self.ingredient_box.grid_forget()
        except: pass
        try: self.clean_timer_screen()
        except: pass
        try: self.clear_recipe_directions_window()
        except: pass
        try: self.the_button.grid_forget()
        except: pass

        self.ri = 0 #reset step idx
        self.show_weather_flag = True

        #spacer1
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("  ")
        self.spacer1 = tk.Label(self.master, textvariable=self.label_text, font=self.spacer_font, bg=background_color)
        self.spacer1.grid(row=roww,column=0)

        self.label_text = tk.StringVar()
        self.label_text.set(" ")
        self.spacer2 = tk.Label(self.master, textvariable=self.label_text, font=self.spacer_font, bg=background_color)
        self.spacer2.grid(row=roww,column=2)

        self.label_text = tk.StringVar()
        self.label_text.set(" ")
        self.spacer3 = tk.Label(self.master, textvariable=self.label_text, font=self.spacer_font, bg=background_color)
        self.spacer3.grid(row=roww,column=4)


        roww+=1
        self.cookin_img=tk.PhotoImage(file="start_cookin_button.png")
        self.start_cookin_button = tk.Button(self.master, text=(), image=self.cookin_img, command=self.draw_start_cookin_window )
        #self.start_cookin_button.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
        #self.start_cookin_button = tk.Button(self.master, text="Start Cookin'",
        #font=self.standard_font, command=self.draw_start_cookin_window)
        self.start_cookin_button.grid(row=roww,column=1)
        self.start_cookin_button.config(bg='DodgerBlue3',fg='sky blue',activebackground = "DodgerBlue3",activeforeground = "sky blue")

        self.timer_img=tk.PhotoImage(file="timer_button.png")
        self.timer_button = tk.Button(self.master, text=(), image=self.timer_img, command=self.draw_timer_window )
        #self.start_cookin_button.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
        self.timer_button.grid(row=roww,column=3)
        self.timer_button.config(bg='DodgerBlue3',fg='sky blue',activebackground = "DodgerBlue3",activeforeground = "sky blue")

        self.cast_img=tk.PhotoImage(file="cast_button.png")
        self.cast_button = tk.Button(self.master, text=(), image=self.cast_img, command=1)
        #self.start_cookin_button.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
        self.cast_button.grid(row=roww,column=5)
        self.cast_button.config(bg='DodgerBlue3',fg='sky blue',activebackground = "DodgerBlue3",activeforeground = "sky blue")


        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(" ")
        self.spacer4 = tk.Label(self.master, textvariable=self.label_text, font=self.spacer_font, bg=background_color)
        self.spacer4.grid(row=roww,column=4)


        roww+=1
        self.create_recipe_img=tk.PhotoImage(file="create_recipe_button.png")
        self.create_recipe_button = tk.Button(self.master, text=(), image=self.create_recipe_img, command=self.draw_create_recipe_window )
        #self.start_cookin_button.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
        self.create_recipe_button.grid(row=roww,column=1)
        self.create_recipe_button.config(bg='DodgerBlue3',fg='sky blue',activebackground = "DodgerBlue3",activeforeground = "sky blue")

        self.weather_img=tk.PhotoImage(file="weather_button.png")
        self.weather_button = tk.Button(self.master, text=(), image=self.weather_img, command=self.weather_window )
        #self.start_cookin_button.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
        self.weather_button.grid(row=roww,column=3)
        self.weather_button.config(bg='DodgerBlue3',fg='sky blue',activebackground = "DodgerBlue3",activeforeground = "sky blue")

        self.create_list_img=tk.PhotoImage(file="shopping_list_button.png")
        self.create_list_button = tk.Button(self.master, text=(), image=self.create_list_img, command=self.draw_create_list_window )
        #self.start_cookin_button.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
        self.create_list_button.grid(row=roww,column=5)
        self.create_list_button.config(bg='DodgerBlue3',fg='sky blue',activebackground = "DodgerBlue3",activeforeground = "sky blue")


        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(" ")
        self.spacer5 = tk.Label(self.master, textvariable=self.label_text, font=self.spacer_font, bg=background_color)
        self.spacer5.grid(row=roww,column=4)


        roww+=1
        self.update_img=tk.PhotoImage(file="update_button.png")
        self.update_button = tk.Button(self.master, text=(), image=self.update_img, command = lambda :  os.system("git pull") )
        #self.start_cookin_button.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
        self.update_button.grid(row=roww,column=1)
        self.update_button.config(bg='DodgerBlue3',fg='sky blue',activebackground = "DodgerBlue3",activeforeground = "sky blue")

        self.restart_img=tk.PhotoImage(file="restart_button.png")
        self.restart_button = tk.Button(self.master, text=(), image=self.restart_img, command = lambda : os.system("sudo reboot" ))
        #self.start_cookin_button.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
        self.restart_button.grid(row=roww,column=5)
        self.restart_button.config(bg='DodgerBlue3',fg='sky blue',activebackground = "DodgerBlue3",activeforeground = "sky blue")


        self.master.after(5000, self.weather_window) # open weather window as like a "screensaver"


    def clean_main_screen(self):

        self.show_weather_flag = False

        self.create_recipe_button.grid_forget()
        self.create_list_button.grid_forget()
        self.start_cookin_button.grid_forget()
        self.timer_button.grid_forget()
        self.cast_button.grid_forget()
        self.restart_button.grid_forget()
        self.weather_button.grid_forget()
        self.update_button.grid_forget()


        self.spacer1.grid_forget()
        self.spacer2.grid_forget()
        self.spacer3.grid_forget()
        self.spacer4.grid_forget()
        self.spacer5.grid_forget()


    def clean_timer_screen(self):

        self.spacer1.grid_forget()
        self.spacer2.grid_forget()
        self.spacer3.grid_forget()
        self.spacer4.grid_forget()

        self.hr_display.grid_forget()
        self.min_display.grid_forget()
        self.sec_display.grid_forget()

        self.hr_plus_button.grid_forget()
        self.hr_minus_button.grid_forget()
        self.min_plus_button.grid_forget()
        self.min_minus_button.grid_forget()
        self.sec_plus_button.grid_forget()
        self.sec_minus_button.grid_forget()

        self.col1.grid_forget()
        self.col2.grid_forget()

        self.reset_button.grid_forget()
        self.start_timer_button.grid_forget()
        self.back_to_start_button.grid_forget()


    # Timer
    def draw_timer_window(self):
        #need to clean up all widgets
        self.clean_main_screen()
        self.continue_timer_flag = True

        #spacer
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("             ")
        self.spacer1 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font,bg=background_color)
        self.spacer1.grid(row=roww,column=0)

        self.label_text = tk.StringVar()
        self.label_text.set("   ")
        self.spacer2 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font,bg=background_color)
        self.spacer2.grid(row=roww,column=2)

        self.label_text = tk.StringVar()
        self.label_text.set("   ")
        self.spacer3 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font,bg=background_color)
        self.spacer3.grid(row=roww,column=4)

        #row 1, time plus
        roww+=1

        self.up_img=tk.PhotoImage(file="up_button.png")
        self.hr_plus_button = tk.Button(self.master, text=(), image=self.up_img,
        font=self.big_font, command=self.timer_hr_plus)
        self.hr_plus_button.grid(row=roww,column=1)#, sticky=tk.N+tk.E+tk.W)
        self.hr_plus_button.config(bg='green2')

        self.min_plus_button = tk.Button(self.master, text=(), image=self.up_img,
        font=self.big_font, command=self.timer_min_plus)
        self.min_plus_button.grid(row=roww,column=3)#, sticky=tk.N+tk.E+tk.W)
        self.min_plus_button.config(bg='green2')

        self.sec_plus_button = tk.Button(self.master, text=(), image=self.up_img,
        font=self.big_font, command=self.timer_sec_plus)
        self.sec_plus_button.grid(row=roww,column=5)#, sticky=tk.N+tk.E+tk.W)
        self.sec_plus_button.config(bg='green2')

        #row 2, time display
        roww+=1
        self.hr_display = tk.Label(self.master, textvariable=self.hr_display_text, font=self.huge_font)
        self.hr_display.grid(row=roww,column=1)

        self.label_text = tk.StringVar()
        self.label_text.set(":")
        self.col1 = tk.Label(self.master, textvariable=self.label_text, font=self.huge_font,bg=background_color, fg='white')
        self.col1.grid(row=roww,column=2)

        self.min_display = tk.Label(self.master, textvariable=self.min_display_text, font=self.huge_font)
        self.min_display.grid(row=roww,column=3)

        self.label_text = tk.StringVar()
        self.label_text.set(":")
        self.col2 = tk.Label(self.master, textvariable=self.label_text, font=self.huge_font,bg=background_color, fg='white')
        self.col2.grid(row=roww,column=4)

        self.sec_display = tk.Label(self.master, textvariable=self.sec_display_text, font=self.huge_font)
        self.sec_display.grid(row=roww,column=5)

        #row 3, time minus
        roww+=1
        self.down_img=tk.PhotoImage(file="down_button.png")
        self.hr_minus_button = tk.Button(self.master, text=(), image=self.down_img,
        font=self.big_font, command=self.timer_hr_minus)
        self.hr_minus_button.grid(row=roww,column=1, sticky=tk.N+tk.E+tk.W)
        self.hr_minus_button.config(bg='green2')

        self.min_minus_button = tk.Button(self.master, text=(), image=self.down_img,
        font=self.big_font, command=self.timer_min_minus)
        self.min_minus_button.grid(row=roww,column=3, sticky=tk.N+tk.E+tk.W)
        self.min_minus_button.config(bg='green2')

        self.sec_minus_button = tk.Button(self.master, text=(), image=self.down_img,
        font=self.big_font, command=self.timer_sec_minus)
        self.sec_minus_button.grid(row=roww,column=5, sticky=tk.N+tk.E+tk.W)
        self.sec_minus_button.config(bg='green2')

        #row 4, spacer
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("   ")
        self.spacer4 = tk.Label(self.master, textvariable=self.label_text, font=self.small_font,bg=background_color)
        self.spacer4.grid(row=roww,column=4)

        #row 5, other buttons
        roww+=1
        self.reset_button = tk.Button(self.master, text="Reset",
        font=self.standard_font, command=self.reset_timer)
        self.reset_button.grid(row=roww,column=1, sticky=tk.N+tk.E+tk.W)
        self.reset_button.config(bg='green2')

        self.start_timer_button = tk.Button(self.master, text="Start",
        font=self.standard_font, command=self.start_timer)
        self.start_timer_button.grid(row=roww,column=3, sticky=tk.N+tk.E+tk.W)
        self.start_timer_button.config(bg='green2')

        self.back_to_start_button = tk.Button(self.master, text="Exit",
        font=self.standard_font, command=self.draw_start_screen)
        self.back_to_start_button.grid(row=roww,column=5, sticky=tk.N+tk.E+tk.W)
        self.back_to_start_button.config(bg='green2')


    # reset timer
    def reset_timer(self):
        self.continue_timer_flag = False
        self.hr_display_text.set("00")
        self.min_display_text.set("00")
        self.sec_display_text.set("00")

    # timer hr plus
    def timer_hr_plus(self):
        #increment hours
        hour = float(self.hr_display_text.get())
        hour = int(hour)+1
        if hour < 10:
            hour = '0'+str(hour)
        self.hr_display_text.set(hour)

    # timer hr minus
    def timer_hr_minus(self):
        #decrement hours
        hour = float(self.hr_display_text.get())
        hour = int(hour)-1
        if hour < 0: hour = 0
        if hour < 10:
            hour = '0'+str(hour)
        self.hr_display_text.set(hour)

    # timer min plus
    def timer_min_plus(self):
        #increment min
        min = float(self.min_display_text.get())
        min = int(min)+1
        if min < 10:
            min = '0'+str(min)
        self.min_display_text.set(min)

    # timer min minus
    def timer_min_minus(self):
        #decrement min
        min = float(self.min_display_text.get())
        min = int(min)-1
        if min < 0: min = 0
        if min < 10:
            min = '0'+str(min)
        self.min_display_text.set(min)

    # timer min plus
    def timer_sec_plus(self):
        #increment min
        sec = float(self.sec_display_text.get())
        sec = int(sec)+1
        if sec < 10:
            sec = '0'+str(sec)
        self.sec_display_text.set(sec)

    # timer min minus
    def timer_sec_minus(self):
        #decrement min
        sec = float(self.sec_display_text.get())
        sec = int(sec)-1
        if sec < 0: sec = 0
        if sec < 10:
            sec = '0'+str(sec)
        self.sec_display_text.set(sec)


    # run timer
    def start_timer(self):

        #calculate time left
        hour = int(self.hr_display_text.get())
        min = int(self.min_display_text.get())
        sec = int(self.sec_display_text.get())
        time_left = hour*60*60+min*60+sec

        #if time left, decrement and update fields before refreshing in 1 second
        if time_left > 0:
            if sec < 1:
                sec = 59
                if min < 1:
                    min = 59
                    if hour < 1:
                       hour = 0
                    else: hour -= 1
                else: min -= 1
            else: sec -= 1
        else:
            sec = 0
            min = 0
            hour = 0

        if sec < 10:
            sec = '0'+str(sec)
        if min < 10:
            min = '0'+str(min)
        if hour < 10:
            hour = '0'+str(hour)

        self.sec_display_text.set(sec)
        self.min_display_text.set(min)
        self.hr_display_text.set(hour)

        if time_left > 0 and self.continue_timer_flag:
            self.master.after(1000, self.start_timer) #update every second



    # Create Recipe
    def draw_create_recipe_window(self):
        #need to clean up all widgets
        self.clean_main_screen()

        print('coming soon!')

        #spacer1, but it's really a label
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("Coming soon!")
        self.spacer1 = tk.Label(self.master, textvariable=self.label_text, font=self.huge_font, bg=background_color,fg='white')
        self.spacer1.grid(row=roww,column=0)

        #button for back to start
        roww+=1
        self.back_to_start_button = tk.Button(self.master, text="Exit",
        font=self.standard_font, command=self.draw_start_screen)
        self.back_to_start_button.grid(row=roww,column=5, sticky=tk.N+tk.E+tk.W)
        self.back_to_start_button.config(bg='green2')


    # Pick Recipe
    def draw_create_list_window(self):
        #need to clean up all widgets
        self.clean_main_screen()

        #spacer
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("              ")
        self.spacer1 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font,bg=background_color)
        self.spacer1.grid(row=roww,column=0)

        roww+=1
        self.select_recipe_img=tk.PhotoImage(file="select_recipe_button.png")
        self.pick_recipe_button = tk.Button(self.master, text=(), image=self.select_recipe_img,
        font=self.standard_font, command=self.draw_pick_recipe_window)
        self.pick_recipe_button.grid(row=roww,column=3)
        self.pick_recipe_button.config(bg='green2')

        self.pick_for_me_img=tk.PhotoImage(file="pick_for_me_button.png")
        self.auto_pick_recipe_button = tk.Button(self.master, text=(), image=self.pick_for_me_img,
        font=self.standard_font, command=self.draw_auto_pick_recipe_window)
        self.auto_pick_recipe_button.grid(row=roww,column=5)
        self.auto_pick_recipe_button.config(bg='green2')


    def clean_create_list(self):
        self.pick_recipe_button.grid_forget()
        self.auto_pick_recipe_button.grid_forget()

        self.spacer1.grid_forget()


    # Pick Recipe
    def draw_pick_recipe_window(self):
        #need to clean up all widgets
        self.clean_create_list()

        #spacer
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("            ")
        self.spacer1 = tk.Label(self.master, textvariable=self.label_text, font=self.spacer_font, bg=background_color)
        self.spacer1.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("Select Recipe")
        self.recipe_select_label = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.recipe_select_label.grid(row=roww,column=1)

        roww+=1
        self.recipe_var1 = tk.StringVar()
        self.recipe_var1.set(self.recipe_name_list[0]) # default value
        self.recipe1 = tk.OptionMenu(self.master, self.recipe_var1, *self.recipe_name_list)
        self.recipe1.grid(row=roww,column=1)
        self.recipe1.configure(font=self.standard_font)
        self.recipe1['menu'].configure(font=self.standard_font)

        self.recipe_var2 = tk.StringVar()
        self.recipe_var2.set(self.recipe_name_list[0]) # default value
        self.recipe2 = tk.OptionMenu(self.master, self.recipe_var2, *self.recipe_name_list)
        self.recipe2.grid(row=roww,column=4)
        self.recipe2.configure(font=self.standard_font)
        self.recipe2['menu'].configure(font=self.standard_font)

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("  ")
        self.spacer2 = tk.Label(self.master, textvariable=self.label_text,font=self.spacer_font, bg=background_color)
        self.spacer2.grid(row=roww,column=0)

        roww+=1
        self.create_shopping_list_button = tk.Button(self.master, text="Create Shopping List",
        font=self.standard_font, command=lambda : self.create_shopping_list_button_cmd(self.recipe_data_list[self.recipe_name_list.index(self.recipe_var1.get())],self.recipe_data_list[self.recipe_name_list.index(self.recipe_var2.get())]))
        self.create_shopping_list_button.grid(row=roww,column=1)
        self.create_shopping_list_button.config(bg='green2')


    def draw_start_cookin_window(self):
        #need to clean up all widgets
        self.clean_main_screen()

        #spacer
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("            ")
        self.spacer1 = tk.Label(self.master, textvariable=self.label_text, font=self.spacer_font, bg=background_color)
        self.spacer1.grid(row=roww,column=0)

        #Label
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("Select Recipe")
        self.recipe_select_label = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font)
        self.recipe_select_label.grid(row=roww,column=1)

        roww+=1
        self.recipe_var = tk.StringVar()
        self.recipe_var.set(self.recipe_name_list[0]) # default value
        self.recipe_menu = tk.OptionMenu(self.master, self.recipe_var, *self.recipe_name_list)
        self.recipe_menu.grid(row=roww,column=1)
        self.recipe_menu.configure(font=self.standard_font)
        self.recipe_menu['menu'].configure(font=self.standard_font)

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("  ")
        self.spacer2 = tk.Label(self.master, textvariable=self.label_text,font=self.spacer_font, bg=background_color)
        self.spacer2.grid(row=roww,column=0)

        roww+=1
        self.recipe_confirm_button = tk.Button(self.master, text="START COOKIN'",
        font=self.standard_font, command=self.draw_recipe_instructions_window)
        self.recipe_confirm_button.grid(row=roww,column=1)
        self.recipe_confirm_button.config(bg='green2')



    # Auto Pick Recipe
    def draw_auto_pick_recipe_window(self):

        #need to clean up all widgets
        self.clean_create_list()

        idx1 = random.randrange(0,len(self.recipe_list))
        idx2 = random.randrange(0,len(self.recipe_list))

        while idx1 == idx2:
            idx2 = random.randrange(0,len(self.recipe_list))

        #spacer1
        roww = 0
        self.label_text = tk.StringVar()
        self.label_text.set("  ")
        self.spacer1 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font, bg=background_color)
        self.spacer1.grid(row=roww,column=0)

        self.label_text = tk.StringVar()
        self.label_text.set(" ")
        self.spacer2 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font, bg=background_color)
        self.spacer2.grid(row=roww,column=2)


        #show recipes
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(self.recipe_data_list[idx1]['name'])
        self.recipe1 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font, fg='white', bg=background_color)
        self.recipe1.grid(row=roww,column=1)

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(self.recipe_data_list[idx2]['name'])
        self.recipe2 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font, fg='white', bg=background_color)
        self.recipe2.grid(row=roww,column=1)


        #space3
        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set("  ")
        self.spacer3 = tk.Label(self.master, textvariable=self.label_text, font=self.spacer_font, bg=background_color)
        self.spacer3.grid(row=roww,column=0)

        #buttons
        roww+=1
        self.auto_pick_recipe_button = tk.Button(self.master, text="(Re)Pick",
        font=self.standard_font, command=self.auto_pick_cmd)
        self.auto_pick_recipe_button.grid(row=roww,column=1)
        self.auto_pick_recipe_button.config(bg='green2')

        roww+=1
        self.label_text = tk.StringVar()
        self.label_text.set(" ")
        self.spacer4 = tk.Label(self.master, textvariable=self.label_text, font=self.standard_font, bg=background_color)
        self.spacer4.grid(row=roww,column=0)

        roww+=1
        self.create_shopping_list_button = tk.Button(self.master, text="Create Shopping List",
        font=self.standard_font, command=lambda : self.create_shopping_list_button_cmd(self.recipe_data_list[idx1],self.recipe_data_list[idx2]))
        self.create_shopping_list_button.grid(row=roww,column=1)
        self.create_shopping_list_button.config(bg='green2')


    # Randomly Select Recipes
    def auto_pick_cmd(self):

        #need to clean up all widgets and then call next draw
        self.recipe1.grid_forget()
        self.recipe2.grid_forget()
        self.auto_pick_recipe_button.grid_forget()
        self.create_shopping_list_button.grid_forget()


        self.spacer1.grid_forget()
        self.spacer2.grid_forget()
        self.spacer3.grid_forget()
        self.spacer4.grid_forget()

        self.draw_auto_pick_recipe_window()


    # Build Shopping List
    def create_shopping_list_button_cmd(self, recipe1, recipe2):

        #clear old widgets
        self.recipe1.grid_forget()
        self.recipe2.grid_forget()
        self.auto_pick_recipe_button.grid_forget()
        self.create_shopping_list_button.grid_forget()

        self.spacer1.grid_forget()
        self.spacer2.grid_forget()
        self.spacer3.grid_forget()
        self.spacer4.grid_forget()

        try: self.recipe_select_label.grid_forget()
        except: pass


        #create list
        grocery_list = {}
        keys = list(recipe1['ingredients'].keys())
        keys+= list(recipe2['ingredients'].keys())
        keys = list(set(keys))
        for key in keys:

            if key in recipe1['ingredients'] and key in recipe2['ingredients']:
                #print(key)
                ingr1 = recipe1['ingredients'][key].split(' ')
                ingr2 = recipe2['ingredients'][key].split(' ')

                if len(ingr1) < 2: units1 = None
                else: units1 = recipe1['ingredients'][key].split(' ')[1]

                if len(ingr2) < 2: units2 = None
                else: units2 = recipe2['ingredients'][key].split(' ')[1]

                amount1 = float(recipe1['ingredients'][key].split(' ')[0])
                amount2 = float(recipe2['ingredients'][key].split(' ')[0])

                if units1 == 'cup':
                    units1 = 'cups'
                if units2 == 'cup':
                    units2 = 'cups'

                if units1 == 'tsp':
                    units1 = 'cups'
                    amount1=amount1*0.0208333
                if units2 == 'tsp':
                    units2 = 'cups'
                    amount2=amount2*0.0208333

                if units1 == 'tbs':
                    units1 = 'cups'
                    amount1=amount1*0.0625

                if units2 == 'tbs':
                    units2 = 'cups'
                    amount2=amount2*0.0625

                if units1 == 'cups':
                    units1 = 'oz'
                    amount1=amount1*8

                if units2 == 'cups':
                    units2 = 'oz'
                    amount2=amount2*8

                if (units1 == units2) or (units1 in units2 or units2 in units1):
                    if units1 == None: units1 = ''
                    else: units1 = ' ' +units1
                    grocery_list[key] = str(round(amount1+amount2,2))+units1
                else:
                    grocery_list[key] = [recipe1['ingredients'][key],recipe2['ingredients'][key]]

            elif key in recipe1['ingredients']:
                grocery_list[key] = recipe1['ingredients'][key]
            elif key in recipe2['ingredients']:
                grocery_list[key] = recipe2['ingredients'][key]


        print(recipe1['name'])
        print(recipe1['ingredients'])
        print(' ')
        print(recipe2['name'])
        print(recipe2['ingredients'])

        print('\nCombined List: ')
        print(grocery_list)

        #button for back to start
        roww = 0
        self.back_to_start_button = tk.Button(self.master, text="Exit",
        font=self.standard_font, command=self.draw_start_screen)
        self.back_to_start_button.grid(row=roww,column=5, sticky=tk.N+tk.E+tk.W)
        self.back_to_start_button.config(bg='green2')

        #text box for recipe
        roww = 0
        self.ingredient_box = ScrolledText(self.master, bg='white', relief=tk.GROOVE, height=10, width=25,
        font=self.standard_font)
        self.ingredient_box.grid(row = roww, column = 1, sticky=tk.N+tk.S+tk.E+tk.W)

        # Inserting Text
        for key in grocery_list.keys():
            print(key)
            print(grocery_list[key])
            self.ingredient_box.insert(tk.INSERT, (key + ": " + grocery_list[key] + "\n"))

        # Making the text read only 
        self.ingredient_box.configure(state ='disabled')


    # recipe instructions
    def draw_recipe_instructions_window(self):
        self.spacer1.grid_forget()
        self.spacer2.grid_forget()

        self.recipe_select_label.grid_forget()
        self.recipe_menu.grid_forget()
        self.recipe_confirm_button.grid_forget()

        recipe = self.recipe_data_list[self.recipe_name_list.index(self.recipe_var.get())]

        #button to move to next step
        roww = 0
        self.recipe_forward_button = tk.Button(self.master, text=">",
        font=self.standard_font, command=self.recipe_forward)
        self.recipe_forward_button.grid(row=roww,column=5, sticky=tk.N+tk.E+tk.W)
        self.recipe_forward_button.config(bg='green2')

        #button to move to previous step
        roww +=1
        self.recipe_backward_button = tk.Button(self.master, text="<",
        font=self.standard_font, command=self.recipe_backward)
        self.recipe_backward_button.grid(row=roww,column=5, sticky=tk.N+tk.E+tk.W)
        self.recipe_backward_button.config(bg='green2')

        #button to go back to start
        roww +=1
        self.back_to_start_button = tk.Button(self.master, text="Exit",
        font=self.standard_font, command=self.draw_start_screen)
        self.back_to_start_button.grid(row=roww,column=5, sticky=tk.N+tk.E+tk.W)
        self.back_to_start_button.config(bg='red')

        #text box for recipe
        roww = 0
        self.recipe_box = ScrolledText(self.master, bg='white', relief=tk.GROOVE, height=10, width=25,
        font=self.standard_font)
        self.recipe_box.grid(row=roww, column=1, rowspan=4, sticky=tk.N+tk.S+tk.E+tk.W)

        # Inserting Text

        self.recipe_box.insert(tk.INSERT, ("Ingredients: "+ "\n"))
        for key in recipe['ingredients'].keys():
            self.recipe_box.insert(tk.INSERT, ("- " + key + " (" + recipe['ingredients'][key] + ") \n"))

        self.recipe_box.insert(tk.INSERT, ("\nDirections: " + "\n"))

        if self.ri < 1: self.ri = 1
        if self.ri > len(recipe['directions']): self.ri = len(recipe['directions'])
        for ri in range(0,self.ri):
            #print(recipe['directions'][ri])
            self.recipe_box.insert(tk.INSERT, (str(ri) + ". " + recipe['directions'][ri] + "\n"))
        self.recipe_box.see("end")
        # Making the text read only 
        self.recipe_box.configure(state ='disabled')


    # recipe instructions
    def recipe_forward(self):
        self.clear_recipe_directions_window()
        self.ri+=1
        self.draw_recipe_instructions_window()

    def recipe_backward(self):
        self.clear_recipe_directions_window()
        self.ri-=1
        self.draw_recipe_instructions_window()


    def clear_recipe_directions_window(self):
            self.spacer1.grid_forget()
            self.spacer2.grid_forget()
            self.recipe_box.grid_forget()
            self.recipe_forward_button.grid_forget()
            self.recipe_backward_button.grid_forget()
            self.back_to_start_button.grid_forget()



    # program close
    def close_program(self):
        self.master.quit()



# program close, but not inside the class
def close_program2():
    root.destroy()

###########################
#MAIN CODE
###########################

print('start main code') #random magic print

#base stuff needed to make it work
root = tk.Tk()
root.geometry("1920x1080")
root.configure(background=background_color)

my_gui = theGUI(root)

root.protocol("WM_DELETE_WINDOW", close_program2)
root.mainloop()


