#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
install.py

Set up the script

Set up for a python3 on a pi 4

Copyright 2020 Sam Pedrotty

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
'''

###########################
#IMPORT MODULES
###########################
import sys
import os




###########################
#MAIN CODE
###########################

#ensure time and locale are set before continuing
usr_in = input('Before running this script, you should set the system locale and time.  Have you done this? [Y/N]: ')

usr_in=str(usr_in)
print(usr_in)

if usr_in != 'y' and usr_in != 'Y' and usr_in != 'yes' and usr_in != 'YES' and usr_in != 'Yes' and usr_in != 'heck yeah':
    print('set the system locale and time, restart the system, and then try again')
    sys.exit()



# begin install
os.system('cp replace_repository.sh ../../replace_repository.sh')
os.system('chmod +x ../../replace_repository.sh')

os.system('sudo apt-get update -y')
os.system('sudo apt-get upgrade -y')

os.system('sudo apt-get --yes install gedit')

os.system('sudo pip3 install pyowm')

#install RPiPlay
install_dir = os.getcwd()
os.chdir('../../')
os.system('sudo apt-get install cmake -y')
os.system('sudo apt-get install libavahi-compat-libdnssd-dev -y')
os.system('sudo apt-get install libplist-dev -y')
os.system('sudo apt-get install libssl-dev -y')
os.system('sudo apt-get install libgstreamer1.0-dev -y')
os.system('sudo apt-get install libgstreamer-plugins-base1.0-dev -y')
os.system('sudo apt-get install gstreamer1.0-libav -y')
os.system('sudo apt-get install gstreamer1.0-vaapi -y')
os.system('sudo apt-get install gstreamer1.0-plugins-bad -y')
os.system('git clone https://github.com/FD-/RPiPlay.git')
os.chdir('RPiPlay')
os.system('mkdir build')
os.chdir('build')
os.system('sudo cmake --DCMAKE_CXX_FLAGS="-O3" --DCMAKE_C_FLAGS="-O3" .. ')
os.system('sudo make install')



#update lxsession autostart to run from boot
'''
print('\n\n\n\n\nTo start from boot, replace the contents of the lxsession autostart file with the below:\n')
print('@lxpanel --profile LXDE-pi')
print('@pcmanfm --desktop --profile LXDE-pi')
print('@lxterminal --command "/home/pi/lcaev/main/startup.sh"')
print('@xscreensaver -no-splash')
print('@xset s off')
print('@xset -dpms')
os.system('sudo gedit /etc/xdg/lxsession/LXDE-pi/autostart 2>/dev/null')
'''
os.system('sudo mv /etc/xdg/lxsession/LXDE-pi/autostart /etc/xdg/lxsession/LXDE-pi/autostart_ORIGINAL')
os.system('sudo cp /home/pi/whats-for-dinner/linux_install/autostart /etc/xdg/lxsession/LXDE-pi/autostart')

#make scripts executable
os.system('sudo chmod +x /home/pi/whats-for-dinner/py_src/whats_for_dinner.py')
os.system('sudo chmod +x /home/pi/whats-for-dinner/startup.sh')

print('\n...done!')

print('\n\nReboot the pi!!\n\n')


