# What's For Dinner?!

This program is intended to capture recipes, provide options for selecting
them, and then assemble grocery lists.

For easy use on windows, simply run the win_whats_for_dinner.py script with Python 3.

To add recipes, add new json files following the format used by the others in the 'recipes' directory.

Because I've gone completely overboard, feel free to put this on a Pi 3B+ or Pi 4.  In that case,
you should pair it with a 7" HDMI touchscreen w/ 1920x1080 resolution.  Run the install.py script
in the 'linux_install' directory.  Then you should be off to the races when you reboot!

